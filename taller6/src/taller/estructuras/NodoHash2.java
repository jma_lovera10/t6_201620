package taller.estructuras;

public class NodoHash2<K extends Comparable<K>,V extends Comparable<V>> {
	
	private K llave;
	private Lista<V> values;
	
	public NodoHash2(K llave, V valor) {
		super();
		this.llave = llave;
		values = new Lista<V>();
	}
	
	public K getLlave() {
		return llave;
	}
	
	public Lista<V> getValores() {
		return values;
	}
	
	public void addValor(V valor) {
		values.addOrdered(valor);
	}
}
