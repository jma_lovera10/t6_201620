package taller.estructuras;

import taller.mundo.Habitante;

public class TablaHash<K extends Comparable<K> ,V> {

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private NodoHash<K, V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		this(500000,5);
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		this.factorCargaMax = factorCargaMax;
		this.capacidad = capacidad;
		count = 0;
		factorCarga = 0;
		tabla = new NodoHash[capacidad];
	}

	public void put(K llave, V valor){
		if(factorCarga>=factorCargaMax)return;
		
		int pos = hash(llave);
		NodoHash<K, V> nodo = tabla[pos];
		if(nodo==null) tabla[pos] = new NodoHash<K, V>(llave, valor);
		else{
			if(nodo.getLlave().compareTo(llave)==0){
				nodo.setValor(valor);
				return;
			}
			while(nodo.getNext()!=null){
				if(nodo.getNext().getLlave().compareTo(llave)==0){
					nodo.getNext().setValor(valor);
					return;
				}
				nodo = nodo.getNext();
			}
			nodo.setNext(new NodoHash<K, V>(llave, valor));
		}
		count++;
		actualizarFactor();
	}

	public V get(K llave){
		int pos = hash(llave);
		V elem = null;
		NodoHash<K, V> actual = tabla[pos];
		while(elem==null){
			if(actual==null)break;
			else if(actual.getLlave().compareTo(llave)==0)elem = actual.getValor();
			else actual = actual.getNext();
		}

		return elem;
	}

	public V delete(K llave){
		int pos = hash(llave);
		V elem = null;
		NodoHash<K, V> actual = tabla[pos];
		if(actual == null) return elem;
		else if(actual.getLlave().compareTo(llave)==0){
			elem =actual.getValor();
			tabla[pos] = tabla[pos].getNext();
		}
		else{
			while(actual.getNext()!=null){
				if(actual.getNext().getLlave().compareTo(llave)==0){
					elem = actual.getNext().getValor();
					actual.setNext(actual.getNext().getNext());
					count--;
					actualizarFactor();
					break;
				}
				actual = actual.getNext();
			}
		}

		return elem;
	}

	private void actualizarFactor() {
		factorCarga = count/capacidad;
	}

	//Hash
	private int hash(K llave)
	{
		return llave.hashCode()%capacidad;
	}
	
	public static void main(String[] args) {
		int val = 0;
		
		TablaHash<Integer, Habitante> t1 = new TablaHash<Integer, Habitante>();
		TablaHash<Integer, Habitante> t2 = new TablaHash<Integer, Habitante>();
		TablaHash<Integer, Habitante> t3 = new TablaHash<Integer, Habitante>();
		TablaHash<Integer, Habitante> t4 = new TablaHash<Integer, Habitante>();
		
		while(val<=2000000){
			String a = ""+((char)val);
			Habitante h = new Habitante(a, a, a, val);
			if(val<500000)t1.put(h.darContactoAcudiente(), h);
			if(val<1000000)t2.put(h.darContactoAcudiente(), h);
			if(val<1500000)t3.put(h.darContactoAcudiente(), h);
			t4.put(h.darContactoAcudiente(), h);
			val++;
		}
		
		System.out.println("Pruebas de tiempo para \"put\"\n");
		long time = 0;
		time = System.nanoTime();
		t1.put(20, new Habitante(" ", " ", " ", 20));
		time = System.nanoTime()-time;
		System.out.println("Tiempo para primer caso: "+time+" ns\n");
		time = System.nanoTime();
		t2.put(20, new Habitante(" ", " ", " ", 20));
		time = System.nanoTime()-time;
		System.out.println("Tiempo para segundo caso: "+time+" ns\n");
		time = System.nanoTime();
		t3.put(20, new Habitante(" ", " ", " ", 20));
		time = System.nanoTime()-time;
		System.out.println("Tiempo para tercer caso: "+time+" ns\n");
		time = System.nanoTime();
		t4.put(20, new Habitante(" ", " ", " ", 20));
		time = System.nanoTime()-time;
		System.out.println("Tiempo para cuarto caso: "+time+" ns\n");
		
		System.out.println("Pruebas de tiempo para \"get\"\n");
		 
		time = System.nanoTime();
		t1.get(20);
		time = System.nanoTime()-time;
		System.out.println("Tiempo para primer caso: "+time+" ns\n");
		time = System.nanoTime();
		t2.get(20);
		time = System.nanoTime()-time;
		System.out.println("Tiempo para segundo caso: "+time+" ns\n");
		time = System.nanoTime();
		t3.get(20);
		time = System.nanoTime()-time;
		System.out.println("Tiempo para tercer caso: "+time+" ns\n");
		time = System.nanoTime();
		t1.get(20);
		time = System.nanoTime()-time;
		System.out.println("Tiempo para cuarto caso: "+time+" ns\n");
	}
}