package taller.estructuras;

import taller.mundo.Habitante;

public class TablaHash2<K extends Comparable<K> ,V extends Comparable<V>> {

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private NodoHash2<K, V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	@SuppressWarnings("unchecked")
	public TablaHash2(){
		this(100000,5);
	}

	@SuppressWarnings("unchecked")
	public TablaHash2(int capacidad, float factorCargaMax) {
		this.factorCargaMax = factorCargaMax;
		this.capacidad = capacidad;
		count = 0;
		factorCarga = 0;
		tabla = new NodoHash2[capacidad];
	}

	public void put(K llave, V valor){
		int pos = hash(llave);
		NodoHash2<K, V> node = tabla[pos];
		if(node==null){
			tabla[pos] = new NodoHash2<K, V>(llave, valor);
			tabla[pos].addValor(valor);
			count++;
			actualizarFactor();
		}else if(node.getLlave().compareTo(llave)==0){
			tabla[pos].addValor(valor);
			count++;
			actualizarFactor();
		}else{
			int i = pos+1;
			while(true){
				i = i>=capacidad? 0:i;
				if(i==pos)break;
				if(tabla[i]==null){
					tabla[i] = new NodoHash2<K, V>(llave, valor);
					tabla[i].addValor(valor);
					count++;
					actualizarFactor();
					break;
				}
				else if(tabla[i].getLlave().compareTo(llave)==0){tabla[i].addValor(valor);actualizarFactor();count++;break;}
				i++;
			}
		}
	}

	public Lista<V> get(K llave){
		int pos = hash(llave);
		if(tabla[pos]!=null&&tabla[pos].getLlave().compareTo(llave)==0)return tabla[pos].getValores();
		int i = pos+1;
		while(true){
			i = i>=capacidad? 0: i;
			if(i==pos)return null;
			if(tabla[i]!=null&&tabla[i].getLlave().compareTo(llave)==0)return tabla[i].getValores();
			i++;
		}
	}

	public Lista<V> delete(K llave){
		Lista<V> valores = null;
		int pos = hash(llave);
		if(tabla[pos]!=null&&tabla[pos].getLlave().compareTo(llave)==0){
			valores = tabla[pos].getValores();
			tabla[pos]=null;
			count--;
		}else{
			int i = pos+1;
			while(true){
				i = i>=capacidad? 0: i;
				if(i==pos)break;
				if(tabla[i]!=null&&tabla[i].getLlave().compareTo(llave)==0){
					valores = tabla[i].getValores();
					tabla[i] = null;
					count--;
					break;
				}
				i++;
			}
		}
		actualizarFactor();
		return valores;
	}

	private void actualizarFactor() {
		factorCarga = count/capacidad;
	}

	private int hash(K llave){
		return Math.abs(llave.hashCode())%capacidad;
	}
	
	public static void main(String[] args) {
		TablaHash2<String, Habitante> t = new TablaHash2<String,Habitante>();
		System.out.println("Prueba \"put\"\n");
		
		Habitante h = new Habitante("Aaron", "Berkley", "2.99,50.3", 12345670);
		t.put(h.darNombre(), h);
		System.out.println(h.toString());
		Habitante h1 = new Habitante("Aaron", "Hamsley", "8,50.3", 76543456);
		t.put(h1.darNombre(), h1);
		System.out.println(h1.toString());
		Habitante h2= new Habitante("Aaron", "Britt", "2.99,10.33", 98765436);
		t.put(h2.darNombre(), h2);
		System.out.println(h2.toString());
		
		System.out.println("Prueba \"get\"\n");
		
		System.out.println(t.get("Aaron"));
		
		System.out.println("Prueba \"delete\"\n");
		
		t.delete("Aaron");
		System.out.println(t.get("Aaron"));
	}
}
