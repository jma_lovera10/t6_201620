package taller.mundo;

public class Habitante implements Comparable<Habitante>{

	/**
	 * Atributo que modela el nombre del habitante
	 */
	private String nombre;
	
	/**
	 * Atributo que modela el apellido del habitante
	 */
	private String apellido;
	
	/**
	 * Atributo que modela la localizacion del habitante
	 */
	private String localizacion;
	
	/**
	 * Atributo que modela el contacto del acudiente del habitante
	 */
	private int contactoAcudiente;
	
	public Habitante(String nombre, String apellido, String localizacion, int contactoAcudiente) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.localizacion = localizacion;
		this.contactoAcudiente = contactoAcudiente;
	}
	
	/**
	 * Método que retorna el nombre del habitante.
	 * @return Nombre del habitante
	 */
	public String darNombre(){
		return nombre;
	}
	
	/**
	 * Método que retorna el apellido del habitante.
	 * @return Apellido del habitante
	 */
	public String darApellido(){
		return apellido;
	}
	
	/**
	 * Método que retorna la localizacion del habitante.
	 * @return Localizacion del habitante
	 */
	public String darLocalizacion(){
		return localizacion;
	}
	
	/**
	 * Método que retorna el contacto del acudiente del habitante.
	 * @return Contacto del acudiente del habitante
	 */
	public int darContactoAcudiente(){
		return contactoAcudiente;
	}
	
	public String toString(){
		return "Nombre: "+nombre+"\nApellido: "+apellido+"\nLocalizacion: "+localizacion+"\nContacto Acudiente: "+contactoAcudiente+"\n";
	}

	@Override
	public int compareTo(Habitante o) {
		return apellido.compareTo(o.darApellido())<0?-1:apellido.compareTo(o.darApellido())>0? 1:0;
	}

}
