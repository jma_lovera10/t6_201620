-Si llega informaci�n para una llave que ya existe en la tabla, lo que ocurre es que
 el valor asignado a esa llave se reemplaza por el nuevo valor entrante. Por otra parte,
 el criterio de crecimiento de la tabla est� fundamentado en el factor de carga, pues si 
 este supera el m�ximo permitido, es necesario incrementar el tama�o de la tabla para disminuir
 dicho factor. De la misma forma, para la funci�n de hash se decidi� utilizar el m�dulo del
 n�mero de acudiente con el tama�o de la lista, esto debido a que cada n�mero es �nico y
 cambia de manera secuencial para cada habitante contenido en el archivo de datos.

-Teniendo en cuenta que la implementaci�n de la tabla se hizo con las consideraciones 
 previamente expuestas, si se agrega m�s datos el tiempo de ejecuci�n aumenta en un factor
 de n/tam, por lo que es casi despreciable. Esto se debe a que los n�meros son secuenciales y 
 �nicos, por lo que se tiene que la complejidad de b�squeda constante asociado al factor de
 carga.

-Si tiene sentido en cuanto se spuede agrupar sem�nticamente una determinada cantidad de objetos
 bajo un criterio, en este caso la llave.

-En la situaci�n en que los valores obtenidos por la funci�n de hash son iguales para todo elemento
 que se quiere insertar, la complejidad es lineal, por lo que es una situaci�n que vuelve
 vulnerable a esta estructura. Por otra parte, en la situaci�n en que se tienen que implementar
 llaves complejas, se tiene un mayor grado de dificultad para implementar una funci�n de hash
 con la que se pueda hacer que la tabla funcione �ptimamente.